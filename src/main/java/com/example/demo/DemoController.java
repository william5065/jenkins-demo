package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Haoran.Hua
 * @Description: TODO
 * @date 2019/10/29 8:00 下午
 */
@RestController
public class DemoController {
    @RequestMapping(value = "/hello")
    public String helloworld() {
        return "Hello World!!!";
    }
}
